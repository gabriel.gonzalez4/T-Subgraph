import { BigInt } from "@graphprotocol/graph-ts"
import {
  Tornado,
  Allowed,
  Approval,
  Disallowed,
  Paused,
  Transfer,
  Unpaused
} from "../generated/Tornado/Tornado"
import { Account, Tornado as TornadoEntity } from "../generated/schema"

export function handleAllowed(event: Allowed): void {
  let allowedAddress = event.params.target

  let account = Account.load(allowedAddress)

  if(!account) {
    account = new Account(allowedAddress)
  }

  account.isAllowed = true

  let tornado = TornadoEntity.load("1")

  if (!tornado) {
    tornado = new TornadoEntity("1")
    tornado.allowedCount = BigInt.fromI32(0)
  }

  tornado.allowedCount = tornado.allowedCount?.plus(BigInt.fromI32(1))
  
  account.save()
  tornado.save()

  // Note: If a handler doesn't require existing field values, it is faster
  // _not_ to load the entity from the store. Instead, create it fresh with
  // `new Entity(...)`, set the fields that should be updated and save the
  // entity back to the store. Fields that were not set or unset remain
  // unchanged, allowing for partial updates to be applied.

  // It is also possible to access smart contracts from mappings. For
  // example, the contract that has emitted the event can be connected to
  // with:
  //
  // let contract = Contract.bind(event.address)
  //
  // The following functions can then be called on this contract to access
  // state variables and other data:
  //
  // - contract.allowance(...)
  // - contract.allowedTransferee(...)
  // - contract.approve(...)
  // - contract.balanceOf(...)
  // - contract.blockTimestamp(...)
  // - contract.bulkResolve(...)
  // - contract.canUnpauseAfter(...)
  // - contract.chainID(...)
  // - contract.decimals(...)
  // - contract.decreaseAllowance(...)
  // - contract.governance(...)
  // - contract.increaseAllowance(...)
  // - contract.name(...)
  // - contract.nonces(...)
  // - contract.paused(...)
  // - contract.resolve(...)
  // - contract.symbol(...)
  // - contract.totalSupply(...)
  // - contract.transfer(...)
  // - contract.transferFrom(...)
}

export function handleApproval(event: Approval): void {}

export function handleDisallowed(event: Disallowed): void {}

export function handlePaused(event: Paused): void {}

export function handleTransfer(event: Transfer): void {}

export function handleUnpaused(event: Unpaused): void {}
